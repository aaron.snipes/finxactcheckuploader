# Finxact Check Uploader
A repo to house documentation, tools, and files so that we can upload and link checks to our seeded Finxact users. This tool uses https://github.com/moov-io/imagecashletter/blob/master/README.md.

# Prerequisites
1. Docker
1. Postman
1. Finxact Data Seeder
   1. npm
   2. node

# Installation
1. install the needed Docker image via `docker pull moov/imagecashletter:latest`
1. run a Docker container w/ that image via `docker run -p 8083:8083 -p 9093:9093 moov/imagecashletter:latest`

## SECTION 1: Creating a check X937 file for uploading to Finxact
1. pull down and `npm i` latest https://gitlab.com/liveoak/packages/finxact-data-generator
1. create some data using one of the Embedded Banking scenarios.  For example, run: `npm run seed -- --file EB/createSingleWithAllDepositsAccounts`
1. find the output from your test run, and either tie it to an existing test user, or create a new test user ([see link](https://jensyn.atlassian.net/wiki/spaces/EB/pages/2642444331/How+to+manually+create+Test+Users+for+Embedded+Banking))
1. now, let's update our check to include a Finxact account number from what we've seeded.
1. create a copy of originalCheckTemplate.json, name it `whatever.JSON`. **DO NOT FORMAT THE COPIED FILE.**
1. go to your output JSON from the data seeder:
   1. find the account number from an account attached to your partyPerson object.  By default, you can find this by searching for a property called `ppSavingsAcct`
   1. copy and paste this account number as the value for every field named `"onUs"` in the copied file
1. upload your updated JSON file via `curl -X POST -H "content-type: application/json" localhost:8083/files/create --data @./<fileNameFromStep5>.json > response.json`
1. take the id field from your response.json, and then call `curl localhost:8083/files/{yourIdGoesHere}/contents > <someFileNameGoesHere>.X937`

## SECTION 2: Uploading the check X937 to Finxact

1. pull the attached postman collection into your Postman client. 
   1. this runs off of the existing Finxact dev environment.
1. open the `Upload X9 File` call and in the body select your converted X937 file created from the last step section#1. Send the request.
1. open the `Process X9 Credit File` call and in the body, select `raw`.  Update the following body as follows:
```json
{
  "fileName": "<uploadedFileNameGoesHere>.X937",
  "isCr": true,
  "runDtm": "<some ISO date in the future, for ex: 2022-11-30T21:42:20.542Z>"
}
```
- I like to go into chrome's inspector mode -> js console, and use the following command to get the ISO date for right now ```new Date().toISOString()```
- I then add 1 minute to that ISO date, and use that for my `runDtm` field in the body from step#3.

### You're done!  You should now have checks linked to the account# referenced in step#6
